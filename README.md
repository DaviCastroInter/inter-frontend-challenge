# Desafio Frontend

## Descrição

Implementar um formulário contendo um *input* do tipo *number* e um botão de *submit*. Ao submeter o formulário, deve ser feita uma requisição para a API [PokeAPI](https://pokeapi.co/) usando o endpoint `https://pokeapi.co/api/v2/pokemon/<number>`. Dentre outras informações, esse endpoint retorna os seguintes dados:

```
// Ex: https://pokeapi.co/api/v2/pokemon/1

{
  name:"bulbasaur"
  sprites: {
    back_default:"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/1.png"
    back_shiny:"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/back/shiny/1.png"
    front_default:"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/1.png"
    front_shiny:"https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/shiny/1.png"
    ...
  }
  ...
}
```

Ao finalizar a requisição deve ser exibido o nome do pokemon e a lista de imagens retornados pela API como no exemplo abaixo:

![result.png](result.png)

## Implementação

- O desafio deve ser resolvido utilizando [React](https://reactjs.org/).
- Deve ser implementado testes unitários para os componentes utilizando a [Testing Library](https://testing-library.com/).
- Sinta-se à vontade para usar qualquer biblioteca extra.
- O layout não precisa ser idêntico ao mostrado acima, ele serve apenas  para ser usado como base. Use sua criatividade.
- O conteúdo do arquivo Readme deve ser substituído por instruções de como instalar e rodar o projeto, executar os testes e demais informações relevantes.
- Apesar de ser um desafio simples, use boas práticas de programação que você utilizaria em um projeto de porte médio/grande.

## Como submeter sua solução

- Criar um repositório público no seu Gitlab 
- Desenvolver a solução
- Enviar o link do seu repositório 
